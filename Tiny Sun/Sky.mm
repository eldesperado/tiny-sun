//
//  Sky.m
//  Tiny Sun
//
//  Created by El Desperado on 2/17/14.
//  Copyright 2014 El Desperado. All rights reserved.
//

#import "Sky.h"
#import "World.h"

@implementation Sky



+(id)createSkyWithTextureSize:(int)textureSize {
    return [[self alloc] initSkyWithTextureSize:textureSize];
}
-(id)initSkyWithTextureSize:(int)size {
    if (self = [super init]) {
        // init with size
        textureSize = size;
        screenHeight = [[CCDirector sharedDirector] winSize].height;
        screenWidth = [[CCDirector sharedDirector] winSize].width;
        // generate sprite
        self.sprite = [self generateSprite];
        [self addChild:self.sprite];
    }
    return self;
}
-(CCSprite*)generateSprite {
    CCTexture2D *texture = [self generateTexture];
    float width = (float)screenWidth / (float)screenHeight * textureSize;
    float heigth = textureSize;
    CGRect textureRect = CGRectMake(0, 0, width, heigth);
    
    CCSprite *sprite = [CCSprite spriteWithTexture:texture rect:textureRect];
    ccTexParams tp = {GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_REPEAT};
	[sprite.texture setTexParameters:&tp];
    sprite.anchorPoint = ccp(1.0f/8.0f, 0);
	sprite.position = ccp(screenWidth/8, 0);
    return sprite;
}
-(CCTexture2D*)generateTexture {
    CCRenderTexture *renderTexture = [CCRenderTexture renderTextureWithWidth:textureSize height:textureSize];
    // Random colors
    ccColor3B c = (ccColor3B){140, 205, 221};
	ccColor4F cf = ccc4FFromccc3B(c);
    [renderTexture beginWithClear:cf.r g:cf.g b:cf.b a:cf.a];
    
    // create gradient
    self.shaderProgram = [[CCShaderCache sharedShaderCache] programForKey:kCCShader_PositionColor];
    CC_NODE_DRAW_SETUP();
    
    float gradientAlpha = 0.3f;
    CGPoint vertices[4];
    ccColor4F colors[4];
    int vertIdx = 0;
    
    vertices[vertIdx++] = CGPointMake(0, 0);
    colors[vertIdx++] = (ccColor4F){1, 1, 1, 0};
    vertices[vertIdx++] = CGPointMake(textureSize, 0);
    colors[vertIdx++] = (ccColor4F){1, 1, 1, 0};
    vertices[vertIdx++] = CGPointMake(0, textureSize);
    colors[vertIdx++] = (ccColor4F){1, 1, 1, gradientAlpha};
    vertices[vertIdx++] = CGPointMake(textureSize, textureSize);
    colors[vertIdx++] = (ccColor4F){1, 1, 1, gradientAlpha};
    
    // adjust for retina
    for (int idx = 0; idx < vertIdx; idx++) {
        vertices[idx].x *= CC_CONTENT_SCALE_FACTOR();
        vertices[idx].y *= CC_CONTENT_SCALE_FACTOR();
    }
    
    ccGLEnableVertexAttribs(kCCVertexAttribFlag_Position  | kCCVertexAttribFlag_Color);
    
    glVertexAttribPointer(kCCVertexAttrib_Position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    glVertexAttribPointer(kCCVertexAttrib_Color, 4, GL_FLOAT, GL_FALSE, 0, colors);
    glBlendFunc(CC_BLEND_SRC, CC_BLEND_DST);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, (GLsizei)vertIdx);
    
    // create smoke noise
    CCSprite *noise = [CCSprite spriteWithFile:@"noise.png"];
    [noise setBlendFunc:(ccBlendFunc){GL_DST_ALPHA, GL_ZERO}];
    noise.position = CGPointMake(textureSize / 2, textureSize / 2);
    // adjust for retina
    noise.scale = (float)textureSize * 512.0f * CC_CONTENT_SCALE_FACTOR();
    ccDrawColor4F(1, 1, 1, 1);
    [noise visit];
    
    [renderTexture end];
    return renderTexture.sprite.texture;
}
-(void)setOffSetX:(float)offSetX {
    if (_offSetX != offSetX) {
        _offSetX = offSetX;
        _sprite.textureRect = CGRectMake(_offSetX, 0, _sprite.textureRect.size.width, _sprite.textureRect.size.height);
    }
}
-(void)setScale:(float)scale {
    if (_scale != scale) {
        const float minScale = (float)screenHeight / (float)textureSize;
        if (scale < minScale) {
            _scale = minScale;
        } else {
            _scale = scale;
        }
    }
}
@end
