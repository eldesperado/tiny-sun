//
//  Terrain.h
//  Tiny Sun
//
//  Created by El Desperado on 2/17/14.
//  Copyright 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "Box2D.h"
#import "GLES-Render.h"

#define kMaxHillKeyPoints 1000
#define kHillSegmentWidth 5
#define kMaxHillVertices 4000
#define kMaxBorderVertices 800
@class World;

@interface Terrain : CCNode {
    CGPoint hillKeyPoints[kMaxHillKeyPoints];
    int idxHillKeyPoints;
    int fromKeyPoint;
    int toKeyPoint;
    CGPoint hillVertices[kMaxHillVertices];
    CGPoint hillTextCoords[kMaxHillVertices];
    int idxHillVertices;
    CGPoint borderVertices[kMaxBorderVertices];
    int idxBorderVertices;
    CCSprite *_sprite;
    int _offsetX;
    b2World *_world;
    b2Body *_body;
    int screenHeight;
    int screenWidth;
    int textureSize;
    GLESDebugDraw * _debugDraw;
}
@property (retain) CCSpriteBatchNode * batchNode;
@property (retain) CCSprite * stripes;

-(id)initTerrainWithWorld:(b2World*)world;
@end
