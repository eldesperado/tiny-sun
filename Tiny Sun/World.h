//
//  HelloWorldLayer.h
//  Tiny Sun
//
//  Created by El Desperado on 2/17/14.
//  Copyright El Desperado 2014. All rights reserved.
//


#import <GameKit/GameKit.h>

// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "Box2D.h"
#import "GLES-Render.h"
#import "Sky.h"
#import "Terrain.h"

//Pixel to metres ratio. Box2D uses metres as the unit for measurement.
//This ratio defines how many pixels correspond to 1 Box2D "metre"
//Box2D is optimized for objects of 1x1 metre therefore it makes sense
//to define the ratio so that your most common object type is 1x1 metre.

// HelloWorldLayer
@interface World : CCLayer
{
	CCTexture2D *resetBTN;	// weak ref
	b2World* world;					// strong ref
	GLESDebugDraw *m_debugDraw;		// strong ref
    int textureSize;
}
@property (readonly) int screenWidth;
@property (readonly) int screenHeight;

// returns a CCScene that contains the World as the only child
+(CCScene *)scene;

@end
