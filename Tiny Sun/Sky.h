//
//  Sky.h
//  Tiny Sun
//
//  Created by El Desperado on 2/17/14.
//  Copyright 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class World;

@interface Sky : CCNode {
    int screenHeight;
    int screenWidth;
    int textureSize;
    float _scale;
    float _offSetX;
}

@property (nonatomic, retain) CCSprite *sprite;
@property (nonatomic) float offSetX;
@property (nonatomic) float scale;

+(id)createSkyWithTextureSize:(int)textureSize;
-(id)initSkyWithTextureSize:(int)size;
@end
