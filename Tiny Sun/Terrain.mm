//
//  Terrain.m
//  Tiny Sun
//
//  Created by El Desperado on 2/17/14.
//  Copyright 2014 El Desperado. All rights reserved.
//

#import "Terrain.h"
#import "Box2DHelper.h"
#import "World.h"

@implementation Terrain
-(void)resetHillVertices {
    CGSize winSize = [CCDirector sharedDirector].winSize;
    
    static int prevFromKeyPointI = -1;
    static int prevToKeyPointI = -1;
    
    // key points interval for drawing
    while (hillKeyPoints[fromKeyPoint+1].x < _offsetX-winSize.width/8/self.scale) {
        fromKeyPoint++;
    }
    while (hillKeyPoints[toKeyPoint].x < _offsetX+winSize.width*9/8/self.scale) {
        toKeyPoint++;
    }
    
    float minY = 0;
    if (winSize.height > 480) {
        minY = (1136 - 1024)/4;
    }
    if (prevFromKeyPointI != fromKeyPoint || prevToKeyPointI != toKeyPoint) {
        
        // vertices for visible area
        idxHillVertices = 0;
        idxBorderVertices = 0;
        CGPoint p0, p1, pt0, pt1;
        p0 = hillKeyPoints[fromKeyPoint];
        for (int i=fromKeyPoint+1; i<toKeyPoint+1; i++) {
            p1 = hillKeyPoints[i];
            
            // triangle strip between p0 and p1
            int hSegments = floorf((p1.x-p0.x)/kHillSegmentWidth);
            float dx = (p1.x - p0.x) / hSegments;
            float da = M_PI / hSegments;
            float ymid = (p0.y + p1.y) / 2;
            float ampl = (p0.y - p1.y) / 2;
            pt0 = p0;
            borderVertices[idxBorderVertices++] = pt0;
            for (int j=1; j<hSegments+1; j++) {
                pt1.x = p0.x + j*dx;
                pt1.y = ymid + ampl * cosf(da*j);
                borderVertices[idxBorderVertices++] = pt1;
                
                hillVertices[idxHillVertices] = CGPointMake(pt0.x, 0 + minY);
                hillTextCoords[idxHillVertices++] = CGPointMake(pt0.x/512, 1.0f);
                hillVertices[idxHillVertices] = CGPointMake(pt1.x, 0 + minY);
                hillTextCoords[idxHillVertices++] = CGPointMake(pt1.x/512, 1.0f);
                
                hillVertices[idxHillVertices] = CGPointMake(pt0.x, pt0.y);
                hillTextCoords[idxHillVertices++] = CGPointMake(pt0.x/512, 0);
                hillVertices[idxHillVertices] = CGPointMake(pt1.x, pt1.y);
                hillTextCoords[idxHillVertices++] = CGPointMake(pt1.x/512, 0);
                
                pt0 = pt1;
            }
            
            p0 = p1;
        }
        
        prevFromKeyPointI = fromKeyPoint;
        prevToKeyPointI = toKeyPoint;
        
        [self createBox2DBody];
    }
}
-(void)createBox2DBody {
    if (_body) {
        _world->DestroyBody(_body);
    }
    
    b2BodyDef bDef;
    bDef.position.Set(0, 0);
    
    _body = _world->CreateBody(&bDef);
    
    b2EdgeShape bEShape;
    b2Vec2 p1, p2;
    
    for (int idx = 0; idx < idxBorderVertices -1; idx++) {
        p1 = b2Vec2(borderVertices[idx].x / [Box2DHelper pointsPerMeter],
                    borderVertices[idx].y / [Box2DHelper pointsPerMeter]);
        p2 = b2Vec2(borderVertices[idx + 1].x / [Box2DHelper pointsPerMeter],
                    borderVertices[idx + 1].y / [Box2DHelper pointsPerMeter]);
        bEShape.Set(p1, p2);
        _body->CreateFixture(&bEShape, 0);
    }
}

-(id)initTerrainWithWorld:(b2World*)world {
    if (self = [super init]) {
        screenHeight = [[CCDirector sharedDirector] winSize].height;
        screenWidth = [[CCDirector sharedDirector] winSize].width;
        _world = world;
        
        self.shaderProgram = [[CCShaderCache sharedShaderCache] programForKey:kCCShader_PositionTexture];
        [self setupDebugDraw];
        [self generateHillKeyPoints];
        [self resetHillVertices];
        
        _offsetX = 0;
    }
    return self;
}
-(void)setupDebugDraw {
    _debugDraw = new GLESDebugDraw([Box2DHelper pointsPerMeter]);
    _world->SetDebugDraw(_debugDraw);
    _debugDraw->SetFlags(GLESDebugDraw::e_shapeBit | GLESDebugDraw::e_jointBit);
}
-(void)generateHillKeyPoints {
    idxHillKeyPoints = 0;
    float x, y, dx, dy, ny;
    
    x = -screenWidth / 4;
    y = 3 / 4 * screenHeight;
    hillKeyPoints[idxHillKeyPoints++] = CGPointMake(x, y);
    
    x = 0;
    y = screenHeight / 2;
    hillKeyPoints[idxHillKeyPoints++] = CGPointMake(x, y);
    
    int minDX = 150, rangeDX = 80;
    int minDY = 60, rangeDY = 60;
    float sign = -1;
    float maxHeight = screenHeight;
    float minHeight = 20;
    
    while (idxHillKeyPoints < kMaxHillKeyPoints - 1) {
        dx = arc4random_uniform(rangeDX) + minDX;
        x += dx;
        dy = arc4random_uniform(rangeDY) + minDY;
        ny = y + dy * sign;
        if (ny > maxHeight) {
            ny = maxHeight;
        }
        if (ny < minHeight) {
            ny = minHeight;
        }
        y = ny;
        sign *= -1;
        hillKeyPoints[idxHillKeyPoints++] = CGPointMake(x, y);
    }
    
    // cliff to end game
    x += minDX + rangeDX;
    y = 0;
    hillKeyPoints[idxHillKeyPoints++] = CGPointMake(x, y);
    
    
    fromKeyPoint = 0;
    toKeyPoint = 0;
}
-(void)generateBorderKeyPoints {
    idxBorderVertices = 0;
    
    CGPoint p0, p1, pt0, pt1;
    
    p0 = hillKeyPoints[0];
    
    for (int idx = 1; idx < idxHillKeyPoints; idx++) {
        p1 = hillKeyPoints[idx];
        
        int nSegment = (floorf(p1.x - p0.x) / kHillSegmentWidth);
        float da = M_PI / nSegment;
        float dx = (p1.x - p0.x) / nSegment;
        
        float midY = (p0.y + p1.y) / 2;
        float ampl = (p1.y - p0.y) / 2;
        
        pt0 = p0;
        if (idxBorderVertices > kMaxBorderVertices) {
            break;
        }
        borderVertices[idxBorderVertices++] = pt0;
        for (int i = 1; i < nSegment + 1; i++) {
            pt1.x = p0.x + i * dx;
            pt1.y = midY + ampl * cosf(da * i);
            borderVertices[idxBorderVertices++] = pt1;
            if (idxBorderVertices > kMaxBorderVertices) {
                break;
            }
            pt0 = pt1;
        }
        
        p0 = p1;
    }
}
-(void)draw {
    CC_NODE_DRAW_SETUP();
    
    ccGLBindTexture2D(_stripes.texture.name);
    ccGLEnableVertexAttribs( kCCVertexAttribFlag_Position | kCCVertexAttribFlag_TexCoords);
    
    ccDrawColor4F(1.0f, 1.0f, 1.0f, 1.0f);
    glVertexAttribPointer(kCCVertexAttrib_Position, 2, GL_FLOAT, GL_FALSE, 0, hillVertices);
    glVertexAttribPointer(kCCVertexAttrib_TexCoords, 2, GL_FLOAT, GL_FALSE, 0, hillTextCoords);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, (GLsizei)idxHillVertices);
}
- (void) setOffsetX:(float)newOffsetX {
    CGSize winSize = [CCDirector sharedDirector].winSize;
    
    _offsetX = newOffsetX;
    self.position = CGPointMake(winSize.width/8-_offsetX*self.scale, 0);
    [self resetHillVertices];
}
@end
