//
//  HelloWorldLayer.mm
//  Tiny Sun
//
//  Created by El Desperado on 2/17/14.
//  Copyright El Desperado 2014. All rights reserved.
//

// Import the interfaces
#import "World.h"

// Not included in "cocos2d.h"
#import "CCPhysicsSprite.h"

// Needed to obtain the Navigation Controller
#import "AppDelegate.h"


enum {
	kTagParentNode = 1,
};


#pragma mark - HelloWorldLayer

@interface World() {
    Terrain *_terrain;
    Sky *_sky;
}
@end

@implementation World

+(CCScene *)scene {
    CCScene *scene = [CCScene node];
    World *layer = [World node];
	[scene addChild: layer];
    return scene;
}
- (void) onEnter {
    [super onEnter];
    _screenWidth = [[CCDirector sharedDirector] winSize].width;
    _screenHeight = [[CCDirector sharedDirector] winSize].height;
    
    
    [self setupWorld];
    _terrain = [[[Terrain alloc] initTerrainWithWorld:world] autorelease];
    [self addChild:_terrain z:1];
    _sky = [[[Sky alloc] initSkyWithTextureSize:1024] autorelease];
    [self addChild:_sky];
    [self generateStripesTexture];
}
-(id)init {
    if (self = [super init]) {
        
    }
    return self;
}

- (void)setupWorld {
    b2Vec2 gravity = b2Vec2(0.0f, -7.0f);
    bool doSleep = true;
    world = new b2World(gravity);
    world->SetAllowSleeping(doSleep);
}
-(void)generateStripesTexture {
    textureSize = 1024;
    CCSprite *stripes = [self stripedSpriteWithTextureSize:textureSize randomNumberOfStripeFrom:6 randomNumberOfStripeTo:14];
    ccTexParams tp2 = {GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_CLAMP_TO_EDGE};
    [stripes.texture setTexParameters:&tp2];
    _terrain.stripes = stripes;
}
-(CCSprite *)stripedSpriteWithTextureSize:(float)texSize
                 randomNumberOfStripeFrom:(int)minStripes randomNumberOfStripeTo:(int)maxStripes {
    // Random number of stripes
    int nStripes = arc4random_uniform(maxStripes - minStripes) + minStripes;
    if (nStripes % 2) {
        nStripes++; // Always be an odd number
    }
    
    CCRenderTexture *renderTexture = [CCRenderTexture renderTextureWithWidth:texSize height:texSize];
	ccColor4F cf = [self randomColor];
    [renderTexture beginWithClear:cf.r g:cf.g b:cf.b a:cf.a];
    
    CGPoint vertices[nStripes * 6];
    ccColor4F colors[nStripes * 6];
    
    float x1, x2, y1, y2, dx, dy;
    ccColor4F color;
    int nVertices = 0;
    
    if (arc4random_uniform(2)) {
        // diagonal stripes
        x1 = -texSize;
        y1 = 0;
        x2 = 0;
        y2 = texSize;
        dx = (float)texSize * 2 / (float)nStripes;
        dy = 0;
        
        for (int idx = 0; idx < nStripes / 2; idx++) {
            color = [self randomColor];
            for (int k = 0; k < 2; k++) {
                for (int i = 0; i < 6; i++) {
                    colors[nVertices + i] = color;
                }
                // To create 2 triangle (0,1,3) and (4,5,6)
                vertices[nVertices++] = CGPointMake(x1 + k * texSize, y1);
                vertices[nVertices++] = CGPointMake(x1 + k * texSize + dx, y1);
                vertices[nVertices++] = CGPointMake(x2 + k * texSize, y2);
                vertices[nVertices] = vertices[nVertices - 3];
                nVertices++;
                vertices[nVertices] = vertices[nVertices - 3];
                nVertices++;
                vertices[nVertices++] = CGPointMake(x2 + k * texSize + dx, y2);
            }
            x1 += dx;
            x2 += dx;
        }
    } else {
        // horizontal stripes
        x1 = 0;
        y1 = 0;
        x2 = texSize;
        y2 = 0;
        dx = 0;
        dy = (float)texSize / (float)nStripes;
        
        for (int idx = 0; idx < nStripes; idx++) {
            color = [self randomColor];
            for (int i = 0; i < 6; i++) {
                colors[nVertices + i] = color;
            }
            // To create 2 triangle (0,1,3) and (4,5,6)
            vertices[nVertices++] = CGPointMake(x1, y1);
            vertices[nVertices++] = CGPointMake(x2, x2);
            vertices[nVertices++] = CGPointMake(x1, y1 + dy);
            vertices[nVertices] = vertices[nVertices - 3];
            nVertices++;
            vertices[nVertices] = vertices[nVertices - 3];
            nVertices++;
            vertices[nVertices++] = CGPointMake(x2, y2 + dy);
            
            y1 += dy;
            y2 += dy;
        }
    }
    
    // adjust for retina
    for (int i = 0; i < nVertices; i++) {
        vertices[i].x *= CC_CONTENT_SCALE_FACTOR();
        vertices[i].y *= CC_CONTENT_SCALE_FACTOR();
    }
    
    self.shaderProgram =
    [[CCShaderCache sharedShaderCache] programForKey:kCCShader_PositionColor];
    
    // Layer 3: Stripes
    CC_NODE_DRAW_SETUP();
    glVertexAttribPointer(kCCVertexAttrib_Position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    glVertexAttribPointer(kCCVertexAttrib_Color, 4, GL_FLOAT, GL_TRUE, 0, colors);
    glDrawArrays(GL_TRIANGLES, 0, (GLsizei)nVertices);
    
    float gradientAlpha = 0.5;
    
    nVertices = 0;
    
    vertices[nVertices] = CGPointMake(0, 0);
    colors[nVertices++] = (ccColor4F){0, 0, 0, 0};
    
    vertices[nVertices] = CGPointMake(texSize, 0);
    colors[nVertices++] = (ccColor4F){0, 0, 0, 0};
    
    vertices[nVertices] = CGPointMake(0, texSize);
    colors[nVertices++] = (ccColor4F){0, 0, 0, gradientAlpha};
    
    vertices[nVertices] = CGPointMake(texSize, texSize);
    colors[nVertices++] = (ccColor4F){0, 0, 0, gradientAlpha};
    
    // adjust for retina
    for (int i = 0; i < nVertices; i++) {
        vertices[i].x *= CC_CONTENT_SCALE_FACTOR();
        vertices[i].y *= CC_CONTENT_SCALE_FACTOR();
    }
    
    glVertexAttribPointer(kCCVertexAttrib_Position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    glVertexAttribPointer(kCCVertexAttrib_Color, 4, GL_FLOAT, GL_TRUE, 0, colors);
    glBlendFunc(CC_BLEND_SRC, CC_BLEND_DST);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, (GLsizei)nVertices);
    
    // layer 3: top highlight
    float highlightAlpha = 0.5f;
	float highlightWidth = textureSize/4;
    nVertices = 0;
    
    vertices[nVertices] = CGPointMake(0, 0);
    colors[nVertices++] = (ccColor4F){1, 1, 0.5f, highlightAlpha};
    
    vertices[nVertices] = CGPointMake(texSize, 0);
    colors[nVertices++] = (ccColor4F){1, 1, 0.5f, highlightAlpha};
    
    vertices[nVertices] = CGPointMake(0, highlightWidth);
    colors[nVertices++] = (ccColor4F){0, 0, 0, 0};
    
    vertices[nVertices] = CGPointMake(texSize, highlightWidth);
    colors[nVertices++] = (ccColor4F){0, 0, 0, 0};
    
    // adjust for retina
    for (int i = 0; i < nVertices; i++) {
        vertices[i].x *= CC_CONTENT_SCALE_FACTOR();
        vertices[i].y *= CC_CONTENT_SCALE_FACTOR();
    }

    // Add noise
    CCSprite *noise = [CCSprite spriteWithFile:@"noise.png"];
    [noise setBlendFunc:(ccBlendFunc){GL_DST_COLOR, GL_ZERO}];
    noise.position = ccp(texSize/2, texSize/2);
    [noise visit];
    
    glVertexAttribPointer(kCCVertexAttrib_Position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    glVertexAttribPointer(kCCVertexAttrib_Color, 4, GL_FLOAT, GL_TRUE, 0, colors);
    glBlendFunc(CC_BLEND_SRC, CC_BLEND_DST);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, (GLsizei)nVertices);
    
    [renderTexture end];
    
    return [CCSprite spriteWithTexture:renderTexture.sprite.texture];
}
-(void)renderTopBorder {
    float borderWidth = 2.0f;
    float borderAlpha = 0.5f;
    
    CGPoint vertices[2];
    int nVertices = 0;
    
    vertices[nVertices++] = CGPointMake(0, borderWidth / 2);
    vertices[nVertices++] = CGPointMake(textureSize, borderWidth / 2);
    
    // adjust for retina
    //    for (int i = 0; i < nVertices; i++) {
    //        vertices[i].x *= CC_CONTENT_SCALE_FACTOR();
    //        vertices[i].y *= CC_CONTENT_SCALE_FACTOR();
    //    }
    
    ccDrawColor4F(0, 0, 0, borderAlpha);
    glVertexAttribPointer(kCCVertexAttrib_Position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDrawArrays(GL_LINE_STRIP, 0, GLsizei(nVertices));
}

-(ccColor4F)randomColor {
	const int minSum = 450;
	const int minDelta = 150;
	int r, g, b, min, max;
	while (true) {
		r = arc4random_uniform(256);
		g = arc4random_uniform(256);
		b = arc4random_uniform(256);
		min = MIN(MIN(r, g), b);
		max = MAX(MAX(r, g), b);
		if (max-min < minDelta) continue;
		if (r+g+b < minSum) continue;
		break;
	}
	return ccc4FFromccc3B(ccc3(r, g, b));
}
@end
