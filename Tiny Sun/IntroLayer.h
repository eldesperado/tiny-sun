//
//  IntroLayer.h
//  Tiny Sun
//
//  Created by El Desperado on 2/17/14.
//  Copyright El Desperado 2014. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"

// HelloWorldLayer
@interface IntroLayer : CCLayer
{
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
