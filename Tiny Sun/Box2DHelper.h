//
//  Box2DHelper.h
//  Tiny Sun
//
//  Created by El Desperado on 2/18/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Box2DHelper : NSObject

+(float)pointsPerMeter;
+(float)metersPerPoint;

+(float)pixelsPerMeter;
+(float)metersPerPixel;

@end
