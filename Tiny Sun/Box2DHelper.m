//
//  Box2DHelper.m
//  Tiny Sun
//
//  Created by El Desperado on 2/18/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#import "Box2DHelper.h"
#import "cocos2d.h"

@implementation Box2DHelper

+(float)pointsPerMeter {
    return 32.0f;
}
+(float)metersPerPoint {
    return (1 / [self pointsPerMeter]);
}

+(float)pixelsPerMeter {
    return ([self pointsPerMeter] * CC_CONTENT_SCALE_FACTOR());
}
+(float)metersPerPixel {
    return (1 / [self pixelsPerMeter]);
}

@end
