//
//  Config.h
//  Tiny Sun
//
//  Created by El Desperado on 2/17/14.
//  Copyright (c) 2014 El Desperado. All rights reserved.
//

#ifndef Tiny_Sun_Config_h
#define Tiny_Sun_Config_h

// Supported Autorotations:
//		None,
//		UIViewController,
//		CCDirector
//
#define kGameAutorotationNone 0
#define kGameAutorotationUIViewController 1

//
// Define here the type of autorotation that you want for your game
//

#if defined(__ARM_NEON__) || TARGET_IPHONE_SIMULATOR
#define GAME_AUTOROTATION kGameAutorotationUIViewController

// ARMv6 (1st and 2nd generation devices): Don't rotate. It is very expensive
#elif __arm__
#define GAME_AUTOROTATION kGameAutorotationNone


// Ignore this value on Mac
#elif defined(__MAC_OS_X_VERSION_MAX_ALLOWED)

#else
#error(unknown architecture)
#endif


#endif
